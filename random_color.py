#!/usr/bin/env python3

"""
author:  Nils Olofsson
email:   nils.olovsson@gmail.com
website: http://nilsolovsson.se
date:    2021-02-28
license: MIT

    return [int(r*256), int(g*256), int(b*256)]

    https://martin.ankerl.com/2009/12/09/how-to-create-random-colors-programmatically/
"""

import numpy as np

# HSV values in [0..1[
# returns [r, g, b] values from 0 to 255
def HSVtoRGB(h, s, v):
    h_i = int(h*6)
    f = h*6 - h_i
    p = v * (1 - s)
    q = v * (1 - f*s)
    t = v * (1 - (1 - f) * s)
    if h_i == 0:
        r, g, b = v, t, p
    if h_i == 1:
        r, g, b = q, v, p
    if h_i == 2:
        r, g, b = p, v, t
    if h_i == 3:
        r, g, b = p, q, v
    if h_i == 4:
        r, g, b = t, p, v
    if h_i == 5:
        r, g, b = v, p, q
    #return [int(r*256), int(g*256), int(b*256)]
    return [r, g, b]

# use golden ratio
golden_ratio_conjugate = 0.618033988749895
#h = np.random.random() # use random start value

def initRandomColor():
    global h
    golden_ratio_conjugate = 0.618033988749895
    np.random.seed(0)
    h = np.random.random() # use random start value

def randomColor():
    global h
    h += golden_ratio_conjugate
    h %= 1
    return HSVtoRGB(h, 0.5, 0.95)
