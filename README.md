**Ear clipping triangulation**
A python implementation of the ear clipping method for triangulation of a simple
polygon in 2D into a set of triangles.

Running the script will create the figures used in the blog post.

Blog post:
http://www.all-systems-phenomenal.com/articles/ear_clipping_triangulation

**License: MIT**
