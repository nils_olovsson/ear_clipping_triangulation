#!/bin/bash

ffmpeg -framerate 5 -pattern_type glob -i './koch-sequence/img/*.png' -c:v libx264 -pix_fmt yuv420p sequence.mp4
