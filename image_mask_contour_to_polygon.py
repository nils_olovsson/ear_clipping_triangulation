
#!/usr/bin/env python3

"""
    image_mask_contour_to_polygon.py
"""

__author__    = 'Nils Olofsson'
__email__     = 'me@nilsolovsson.se'
__copyright__ = 'Copyright 2021, AllSystemsPhenomenal'
__license__   = 'MIT'

import skimage.io
import numpy as np

class ImageMaskContourToPolygon:
    """
        Convert a nicely shaped image edge mask to a sequence
        of vertices.
        The edge mask must be a "thin" edge mask so that pixels
        have a valid next neighbor when traversing.
        This can only be assured here before executing the code
        and by manually editing such an image. Sorry.
    """

    def __init__(self, filename):
        self.image = skimage.io.imread(filename, as_gray=True)
        self.visited = np.zeros(self.image.shape, dtype=np.uint8)
        self.pos = np.array([0,0], dtype=int)

    def findStart(self):
        (r, c) = self.image.shape
        for i in range(0, r):
            for j in range(0, c):
                if self.image[i,j] != 0.0:
                    self.pos[0] = i
                    self.pos[1] = j
                    return self.pos

    def validPos(self, pos):
        """
            Test if an image position is valid for next pos.
        """
        (h,w) = self.image.shape
        if pos[0]<0 or pos[0]>h-1:
            return False
        if pos[1]<0 or pos[1]>w-1:
            return False
        value = self.image[pos[0], pos[1]] > 0.0
        visited = self.visited[pos[0], pos[1]] > 0
        if value and not visited:
            return True
        else:
            return False

    def next(self):
        """
           The next position is found by testing neighbors in this order.
           It is rather arbitraty, just to make sure we move in a counter
           clockwise order at start.
             x →
           y ┌───┐
           ↓ │765│
             │4p2│
             │301│
             └───┘
        """
        (h,w) = self.image.shape

        offset = np.zeros([8,2], dtype=int)
        #                        y  x
        offset[0,:] = np.array([ 1, 0], dtype=int)
        offset[1,:] = np.array([ 1, 1], dtype=int)
        offset[2,:] = np.array([ 0, 1], dtype=int)
        offset[3,:] = np.array([ 1,-1], dtype=int)
        offset[4,:] = np.array([ 0,-1], dtype=int)
        offset[5,:] = np.array([-1, 1], dtype=int)
        offset[6,:] = np.array([-1, 0], dtype=int)
        offset[7,:] = np.array([-1,-1], dtype=int)

        for i in range(0,8):
            npos = self.pos + offset[i]
            if self.validPos(npos):
                self.pos = npos
                return
        raise Exception('ImageMaskContourToPolygon.next()', 'Next vertex not found')

    def execute(self):
        """
            Convert the image to a list of counter clockwise vertices.
        """
        nr_verts = np.count_nonzero(self.image)-1
        verts = np.zeros([nr_verts, 2])

        self.findStart()
        self.visited[self.pos[0], self.pos[1]] = 1
        verts[0,1] = self.pos[0]
        verts[0,0] = self.pos[1]
        for i in range(1, nr_verts):
            self.next()
            self.visited[self.pos[0], self.pos[1]] = 1
            verts[i,1] = float(self.pos[0])
            verts[i,0] = float(self.pos[1])
        return verts
