#!/usr/bin/env python3

"""
author:  Nils Olofsson
email:   nils.olovsson@gmail.com
website: http://nilsolovsson.se
date:    2021-02-28
license: MIT

"""

import os
import copy

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import matplotlib.ticker as ticker

import matplotlib.patches as mpatches

from mpl_toolkits.mplot3d import Axes3D
from matplotlib import rc
from matplotlib import animation

from random_color import initRandomColor, randomColor

from double_linked_list import Node, DoubleLinkedList

from image_mask_contour_to_polygon import ImageMaskContourToPolygon

# ----------------------------------------------------------
# Set global background colors for all plots so it fits
# publishing backgound.
# Usually #ffffff or a softer color #f5f5f0.
#
# Colors of indivual plots can be set by:
#   fig.patch.set_facecolor(__COLOR__)
#   ax = fig.gca()
#   ax.set_facecolor(__COLOR__)
# ----------------------------------------------------------

color_table = {}
color_table['blue'] = '#CCCCFF'
color_table['red'] = '#FFAAAA'
color_table['bkg'] = '#FFFFFF'
#color['bkg'] = '#ff00ff'

ax_color = color_table['bkg']

plt.rcParams['figure.facecolor'] = color_table['bkg']
plt.rcParams['axes.facecolor'] = ax_color

def mkdir(dirs):
    """
        If directories and subdirectories do not exist, create them.
        dirs is a list of subdirectory names,
        mkdir(['foo', 'bar']) -> /foo/bar/
    """
    directory = ''
    for d in dirs:
        directory = os.path.join(directory, d)
        if not os.path.isdir(directory):
            os.mkdir(directory, 0o0755)
    return directory

# ==============================================================================
#
# Geometry
#
# ==============================================================================

def getExtents(vertices):
    """
        Get the extents, axis aligned bounding box, of the set of vertices.
    """

    box = np.zeros([4,1])
    box[0] = np.min(vertices[:,0]) # left
    box[1] = np.min(vertices[:,1]) # bottom
    box[2] = np.max(vertices[:,0]) # right
    box[3] = np.max(vertices[:,1]) # top
    return box

def vertexNormal(v0, v1, v2):
    """
        Compute the 2D vertex normal, for placing rendered vertex numbers.
    """
    e0 = v1 - v0
    e1 = v2 - v1
    n0 = np.array([e0[1], -e0[0]])
    n1 = np.array([e1[1], -e1[0]])
    l0 = np.linalg.norm(e0)
    l1 = np.linalg.norm(e1)
    
    #n = n0 + n1
    #n = l0*n0 + l1*n1
    n = 0.5*(1.0/l0*n0 + 1.0/l1*n1)
    n = 1.0 / np.linalg.norm(n) * n
    return n

def computeNormals(vertices):
    """
        Compute the 2D vertex normals of the polygon, for placing rendered
        vertex numbers.
    """

    n,m = vertices.shape
    normals = np.zeros(vertices.shape)
    for i in range(n):
        v0 = vertices[i-1,:]
        v1 = vertices[i,:]
        v2 = vertices[(i+1)%n,:]
        normals[i,:] = vertexNormal(v0,v1,v2)

    return normals

def angleCCW(a, b):
    """
        Counter clock wise angle (radians) from normalized 2D vectors a to b
    """
    dot = a[0]*b[0] + a[1]*b[1]
    det = a[0]*b[1] - a[1]*b[0]
    angle = np.arctan2(det, dot)
    if angle<0.0 :
        angle = 2.0*np.pi + angle
    return angle

def isConvex(vertex_prev, vertex, vertex_next):
    """
        Determine if vertex lies on the convex hull of the polygon.
    """
    a = vertex_prev - vertex
    b = vertex_next - vertex
    internal_angle = angleCCW(b, a)
    return internal_angle <= np.pi

def insideTriangle(a, b, c, p):
    """
        Determine if a vertex p is inside (or "on") a triangle made of the
        points a->b->c
        http://blackpawn.com/texts/pointinpoly/
    """

    #Compute vectors
    v0 = c - a
    v1 = b - a
    v2 = p - a

    # Compute dot products
    dot00 = np.dot(v0, v0)
    dot01 = np.dot(v0, v1)
    dot02 = np.dot(v0, v2)
    dot11 = np.dot(v1, v1)
    dot12 = np.dot(v1, v2)

    # Compute barycentric coordinates
    denom = dot00*dot11 - dot01*dot01
    if abs(denom) < 1e-20:
        return True
    invDenom = 1.0 / denom
    u = (dot11*dot02 - dot01*dot12) * invDenom
    v = (dot00*dot12 - dot01*dot02) * invDenom

    # Check if point is in triangle
    return (u >= 0) and (v >= 0) and (u + v < 1)

def triangulate(vertices, max_iterations=0):
    """
        Triangulation of a polygon in 2D.
        Assumption that the polygon is simple, i.e has no holes, is closed and
        has no crossings and also that it the vertex order is counter clockwise.
        https://geometrictools.com/Documentation/TriangulationByEarClipping.pdf
    """

    n, m = vertices.shape
    indices = np.zeros([n-2, 3], dtype=np.int)

    #print('shape: {}x{}'.format(n,m))

    vertlist = DoubleLinkedList()
    for i in range(0, n):
        vertlist.append(i)

    index_counter = 0
    it_counter = 0

    # Simplest possible algorithm. Create list of indexes.
    # Find first ear vertex. Create triangle. Remove vertex from list
    # Do this while number of vertices > 2.
    node = vertlist.first
    #while vertlist.size > 2 and it_counter < 10:
    while vertlist.size > 2 and (max_iterations<=0 or max_iterations>index_counter):
        #print(it_counter)
        #print('vertlist.size: {}'.format(vertlist.size))
        i = node.prev.data
        j = node.data
        k = node.next.data

        vert_prev = vertices[i,:]
        vert_crnt = vertices[j,:]
        vert_next = vertices[k,:]

        is_convex = isConvex(vert_prev, vert_crnt, vert_next)
        is_ear = True
        if is_convex:
            test_node = node.next.next
            while test_node!=node.prev and is_ear:
                vert = vertices[test_node.data,:]
                is_ear = not insideTriangle(vert_prev, vert_crnt, vert_next, vert)
                test_node = test_node.next
        else:
            is_ear = False

        if is_ear:
            indices[index_counter, :] = np.array([i, j, k], dtype=np.int)
            index_counter += 1
            vertlist.remove(node.data)
        it_counter += 1
        node = node.next
    indices = indices[0:index_counter, :]
    return indices, vertlist.flatten()

# ==============================================================================
#
# Draw polygon
#
# ==============================================================================

def drawInternalAngle(triplet, fig, t=0.2, z_offset=0):
    """
    """
    vp = triplet[0]
    v0 = triplet[1]
    vn = triplet[2]

    a = vp - v0
    b = vn - v0

    l0 = np.linalg.norm(a)
    l1 = np.linalg.norm(b)

    r = t * min(l0, l1)

    theta1 = 180 / np.pi * np.arctan2(b[1], b[0])
    theta2 = 180 / np.pi * np.arctan2(a[1], a[0])

    ax = fig.gca()
    patch = mpatches.Arc(v0,
                         2*r,
                         2*r,
                         angle=0,
                         theta1=theta1,
                         theta2=theta2,
                         linewidth=3.0,
                         linestyle=':',
                         zorder=z_offset)
    ax.add_patch(patch)

def drawConnectingEdge(triplet, fig, z_offset=0):
    """
    """
    vp = triplet[0]
    v0 = triplet[1]
    vn = triplet[2]

    ax = fig.gca()
    l = mlines.Line2D([vp[0],vn[0]], [vp[1],vn[1]])
    l.set_color('black')
    l.set_linewidth(3)
    l.set_linestyle('--')
    l.set_zorder(z_offset)
    l.set_solid_capstyle('round')
    ax.add_line(l)

def drawFace(triplet, fig, color, z_offset=0):
    """
    """
    x = np.zeros(3)
    y = np.zeros(3)
    x[0] = triplet[0][0]
    x[1] = triplet[1][0]
    x[2] = triplet[2][0]

    y[0] = triplet[0][1]
    y[1] = triplet[1][1]
    y[2] = triplet[2][1]

    plt.fill(x,y, color=color, zorder=z_offset)

def getDefaultOptions():
    options = {}
    options['face_color'] = color_table['blue']
    options['max_vertices'] = None
    options['draw_polygon'] = True
    options['draw_vertices'] = True
    options['draw_edges'] = False
    options['draw_faces'] = True
    options['annotate_faces'] = False
    options['annotate_verts'] = False
    return options

def drawTriangulation(vertices, normals, indices,
                      name='polygon', directory='./', colors=[], fig=None,
                      options={}):
    """
    """

    # Plot options
    draw_vertices  = True
    draw_polygon   = True
    draw_edges     = True
    draw_faces     = True
    face_color     = None
    annotate_verts = True
    annotate_faces = False
    max_vertices   = None
    z_offset       = 0

    if not 'annotate-index-as' in options.keys():
        options['annotate-index-as'] = {}
    if not 'reverse-vertex-annotation' in options.keys():
        options['reverse-vertex-annotation'] = False

    if 'draw_vertices' in options:
        draw_vertices = options['draw_vertices']
    if 'draw_polygon' in options:
        draw_polygon = options['draw_polygon']
    if 'draw_edges' in options:
        draw_edges = options['draw_edges']
    if 'draw_faces' in options:
        draw_faces = options['draw_faces']
    if 'face_color' in options:
        face_color = options['face_color']
    if 'annotate_verts' in options:
        annotate_verts = options['annotate_verts']
    if 'annotate_faces' in options:
        annotate_faces = options['annotate_faces']
    if 'max_vertices' in options:
        max_vertices = options['max_vertices']

    if not 'continue' in options.keys():
        options['continue'] = False
    if not 'save' in options.keys():
        options['save'] = False
    if 'z-offset' in options.keys():
        z_offset = options['z-offset']


    # Initialize the figure
    extents = getExtents(vertices)
    my_dpi=96
    #fig = plt.figure(figsize=(800/my_dpi, 800/my_dpi), dpi=my_dpi)
    if not fig:
        fig = plt.figure(figsize=(12, 12))
    ax = fig.gca()

    fig.patch.set_facecolor(color_table['bkg'])
    ax.set_facecolor(ax_color)

    ax.axis('off')

    n,m = vertices.shape
    k = 0
    if len(indices)>0:
        k,_ = indices.shape

    # Draw outer polygon
    if draw_polygon:
        for i in range(0, n):
            j = i+1;
            if(i==n-1):
                j = 0
            p0 = vertices[i,:]
            p1 = vertices[j,:]
            l = mlines.Line2D([p0[0],p1[0]], [p0[1],p1[1]])
            l.set_color('black')
            linewidth = 3.0
            if 'polygon-line-width' in options.keys():
                linewidth = options['polygon-line-width']
            l.set_linewidth(3)
            l.set_zorder(1 + z_offset)
            l.set_solid_capstyle('round')
            ax.add_line(l)

    # Draw vertices
    if draw_vertices:
        if max_vertices:
            ax.scatter(vertices[0:max_vertices,0],
                       vertices[0:max_vertices,1],
                       s=80,
                       zorder=3+z_offset,
                       edgecolors='black',
                       color='black')
        else:
            ax.scatter(vertices[:,0],
                       vertices[:,1],
                       s=80,
                       zorder=3+z_offset,
                       edgecolors='black',
                       color='black')
    # Number vertices
    if annotate_verts:
        num_verts = n
        if max_vertices:
            num_verts = max_vertices
        for i in range(num_verts):
            j = i+1;
            if(i==n-1):
                j = 0
            p0 = vertices[i-1,:]
            p1 = vertices[i,:]
            p2 = vertices[j,:]
            normal = normals[i,:]
            offset = 2*0.0175
            p = p1 + offset*normal + np.array([-0.02, -0.01])
            #p = p1 + np.array([-0.005, -0.005])
            index = i
            if i in options['annotate-index-as'].keys():
                index = options['annotate-index-as'][i]
            if options['reverse-vertex-annotation']:
                index = num_verts - index - 1
            if 'offset-vertex-annotation' in options.keys():
                index += options['offset-vertex-annotation']
            plt.text(p[0], p[1], str(index), fontsize=24)

            #pn = p1 + offset*normal
            #l = mlines.Line2D([p1[0],pn[0]], [p1[1],pn[1]])
            #l.set_color('red')
            #l.set_linewidth(4)
            #l.set_solid_capstyle('round')
            #ax.add_line(l)

    # Draw triangles
    for i in range(0, k):
        x = np.zeros(3)
        y = np.zeros(3)
        x[0] = vertices[indices[i,0],0]
        x[1] = vertices[indices[i,1],0]
        x[2] = vertices[indices[i,2],0]

        y[0] = vertices[indices[i,0],1]
        y[1] = vertices[indices[i,1],1]
        y[2] = vertices[indices[i,2],1]

        if draw_edges:
            for e in range(0, 3):
                j = e+1;
                if(e==3-1):
                    j = 0
                p0 = [x[e], y[e]]
                p1 = [x[j], y[j]]
                l = mlines.Line2D([p0[0],p1[0]], [p0[1],p1[1]])
                l.set_color('black')
                linewidth = 2.0
                if 'edge-line-width' in options.keys():
                    linewidth = options['edge-line-width']
                l.set_linewidth(linewidth)
                l.set_zorder(2 + z_offset)
                l.set_solid_capstyle('round')
                ax.add_line(l)

        if annotate_faces:
            c = 1.0/3.0 * np.array([x.sum(), y.sum()])
            plt.text(c[0], c[1], chr(65+i), fontsize=24)

        if draw_faces:
            if face_color:
                plt.fill(x,y, color=face_color, zorder = 0 + z_offset)
            else:
                color = randomColor()
                if colors and len(colors)>i:
                    color = colors[i]
                plt.fill(x,y, color=color, zorder = 0 + z_offset)

    #plt.subplots_adjust(top = 1.5, bottom = -0.5, right = 1.5, left = -0.5,  hspace = 0, wspace = 0)
    plt.gca().margins(0.01, 0.01)
    plt.gca().xaxis.set_major_locator(ticker.NullLocator())
    plt.gca().yaxis.set_major_locator(ticker.NullLocator())

    if 'xlim' in options.keys():
        ax.set_xlim(options['xlim'][0], options['xlim'][1])
    if 'ylim' in options.keys():
        ax.set_ylim(options['ylim'][0], options['ylim'][1])

    #plt.subplots_adjust(top = 3, bottom = -3, right = 3, left = -3,  hspace = 0, wspace = 0)

    if options['save']:
        filedir = [directory, 'img']
        filedir = mkdir(filedir)
        filepath = os.path.join(filedir, '{}.png'.format(name))
        print("Saved to: " + filepath)
        #plt.tight_layout()
        plt.savefig(filepath, bbox_inches='tight',
                    facecolor=fig.get_facecolor(), edgecolor='none')

        filedir = [directory, 'svg']
        filedir = mkdir(filedir)
        filepath = os.path.join(filedir, '{}.svg'.format(name))
        print("Saved to: " + filepath)
        plt.savefig(filepath, bbox_inches='tight',
                    facecolor=fig.get_facecolor(), edgecolor='none')

    if not options['continue']:
        plt.close()
    return fig

# ==============================================================================
#
# Polygon generation
#
# ==============================================================================

def generateSimple():
    """
        Generate a simply bounded polygon
    """

    vertices = np.zeros([15, 2])
    vertices[0,:] = np.array([0.10, 0.20])
    vertices[1,:] = np.array([0.20, 0.10])
    vertices[2,:] = np.array([0.30, 0.15])
    vertices[3,:] = np.array([0.50, 0.05])
    vertices[4,:] = np.array([0.70, 0.30])
    vertices[5,:] = np.array([0.60, 0.40])
    vertices[6,:] = np.array([0.50, 0.35])
    
    vertices[7,:]  = np.array([0.45, 0.50])
    vertices[8,:]  = np.array([0.40, 0.40])
    vertices[9,:]  = np.array([0.35, 0.47])
    vertices[10,:] = np.array([0.42, 0.60])
    vertices[11,:] = np.array([0.37, 0.65])
    vertices[12,:] = np.array([0.30, 0.55])
    vertices[13,:] = np.array([0.25, 0.35])
    vertices[14,:] = np.array([0.15, 0.30])
    
    # Scale it so bounding box is 1x1
    vertices = 10.0/6.0 * vertices
    
    normals = computeNormals(vertices)
    indices, _ = triangulate(vertices)
    
    return vertices, normals, indices

def generateSimpleAlt():
    """
        Generate a simply bounded polygon
    """

    vertices = np.zeros([7, 2])
    vertices[0,:] = np.array([0.00, 0.05])
    vertices[1,:] = np.array([0.30, 0.09])
    vertices[2,:] = np.array([0.40, 0.20])
    vertices[3,:] = np.array([0.60, 0.00])
    vertices[4,:] = np.array([0.50, 0.40])
    vertices[5,:] = np.array([0.25, 0.095])
    vertices[6,:] = np.array([0.10, 0.30])

    # Scale it so bounding box is 1x1
    vertices[:,1] = 0.6/0.4 * vertices[:,1]

    normals = np.zeros([7,2])
    normals[0,:] = np.array([-0.1,-0.3])
    normals[1,:] = np.array([ 0.2,-0.6])
    normals[2,:] = np.array([ 0.3,-0.8])
    normals[3,:] = np.array([-0.3,-0.2])
    normals[4,:] = np.array([-0.3, 0.1])
    normals[5,:] = np.array([ 0.3, 0.9])
    normals[6,:] = np.array([-0.1, 0.0])
    indices, _ = triangulate(vertices)

    #normals = []
    #indices = []
    return vertices, normals, indices

def generateNoneSimple():
    """
        Generate a nonsimple polygon with a vertex fan
    """
    vertices = np.zeros([5, 2])
    vertices[0,:] = np.array([0.00, 0.00])
    vertices[1,:] = np.array([0.51, 0.53])
    vertices[2,:] = np.array([1.00, 0.05])
    vertices[3,:] = np.array([0.95, 0.97])
    vertices[4,:] = np.array([0.05, 1.0])
    
    normals = np.zeros([5, 2])
    normals[0,:] = np.array([-np.sqrt(0.5), -np.sqrt(0.5)])
    normals[1,:] = np.array([0.00,-1.00])
    normals[2,:] = np.array([np.sqrt(0.5), -np.sqrt(0.5)])
    normals[3,:] = np.array([np.sqrt(0.5),  np.sqrt(0.5)])
    normals[4,:] = np.array([-np.sqrt(0.5), np.sqrt(0.5)])

    indices = np.zeros([2, 3], dtype=np.int)
    indices[0,:] = np.array([0,1,4])
    indices[1,:] = np.array([1,2,3])

    return vertices, normals, indices

def generateCrossing():
    """
        Generate a nonsimple polygon with edges that cross
    """
    vertices = np.zeros([7, 2])
    vertices[1,:] = np.array([0.00, 0.10])
    vertices[0,:] = np.array([0.10, 0.00])
    vertices[2,:] = np.array([1.00, 0.05])
    vertices[3,:] = np.array([0.15, 1.00])
    vertices[4,:] = np.array([0.90, 0.80])
    
    vertices[5,:] = np.array([0.60, 0.50])
    vertices[6,:] = np.array([0.19, 0.09])
    
    normals = np.zeros([7, 2])
    normals[0,:] = np.array([1.00, 0.00])
    normals[1,:] = np.array([0.00, 1.00])
    normals[2,:] = np.array([0.00,-1.00])
    normals[3,:] = np.array([-1.00, 0.00])
    normals[4,:] = np.array([0.00, 1.00])
    normals[5,:] = np.array([0.00, 0.00])
    normals[6,:] = np.array([0.00, 0.00])

    indices = np.zeros([3, 3], dtype=np.int)
    indices[0,:] = np.array([0,6,1])
    indices[1,:] = np.array([6,2,5])
    indices[2,:] = np.array([5,4,3])

    return vertices, normals, indices

def generateBox():
    """
    """

    vertices = np.zeros([8, 2])
    vertices[6,:] = np.array([0.0, 0.0])
    vertices[7,:] = np.array([1.0, 0.0])
    vertices[0,:] = np.array([2.0, 0.0])
    vertices[1,:] = np.array([3.0, 0.0])
    
    vertices[2,:] = np.array([3.0, 1.0])
    vertices[3,:] = np.array([2.0, 1.0])
    vertices[4,:] = np.array([1.0, 1.0])
    vertices[5,:] = np.array([0.0, 1.0])
    
    normals = computeNormals(vertices)
    indices, _ = triangulate(vertices)
    
    return vertices, normals, indices

def generateCircle():
    """
        Generate a low res circle that is put on top of other shape to
        exemplify a polygon with another polygon inside.
    """
    n = 6
    r = 1
    vertices = np.zeros([n,2])
    normals  = np.zeros([n,2])

    a = 0.0
    for i in range(n):
        vertices[i,0] = 0.15*np.cos(a) + 0.8
        vertices[i,1] = 0.15*np.sin(a) + 0.4
        normals[i,0] = np.cos(a)
        normals[i,1] = np.sin(a)
        a += 2*np.pi / n
    indices, _  = triangulate(vertices)

    return vertices, normals, indices

def kochSnowflake(order=3, scale=0.5):
    """
    Return nx2 matrix of point coordinates of the Koch snowflake.
    https://matplotlib.org/3.1.1/gallery/lines_bars_and_markers/fill.html

    Arguments
    ---------
    order : int
        The recursion depth.
    scale : float
        The extent of the snowflake (edge length of the base triangle).
    """
    def _koch_snowflake_complex(order):
        if order == 0:
            # initial triangle
            angles = np.array([0, 120, 240]) + 90
            return scale / np.sqrt(3) * np.exp(np.deg2rad(angles) * 1j)
        else:
            ZR = 0.5 - 0.5j * np.sqrt(3) / 3

            p1 = _koch_snowflake_complex(order - 1)  # start points
            p2 = np.roll(p1, shift=-1)  # end points
            dp = p2 - p1  # connection vectors

            new_points = np.empty(len(p1) * 4, dtype=np.complex128)
            new_points[::4] = p1
            new_points[1::4] = p1 + dp / 3
            new_points[2::4] = p1 + dp * ZR
            new_points[3::4] = p1 + dp / 3 * 2
            return new_points

    points = _koch_snowflake_complex(order)
    
    vertices = np.array([points.real, points.imag])
    vertices = vertices.transpose()
    
    return vertices


# ==============================================================================
#
# Examples, creating image illustrations
#
# ==============================================================================

def exampleSimpleAlt():
    """
        Simple polygon example. The shape has a few vertices and we create
        images that show how first triangle is created and why some are not
        valid.
    """
    options = getDefaultOptions()

    vertices, normals, indices = generateSimpleAlt()

    n = len(indices)
    options['draw_polygon'] = True
    options['draw_vertices'] = True
    options['draw_edges'] = True
    options['draw_faces'] = True
    options['annotate_faces'] = False
    options['annotate_verts'] = True
    options['save'] = True
    options['continue'] = False

    options['xlim'] = [-0.05, 0.65]
    options['ylim'] = [-0.05, 0.65]

    directory = 'simple-alt'

    # ------------------------------------------------------
    # Create filled polygon
    # ------------------------------------------------------
    fig = drawTriangulation(vertices, normals, indices, 'simple-alt', directory, options=options)
    options['draw_edges'] = False
    fig = drawTriangulation(vertices, normals, indices, 'simple-alt-filled', directory, options=options)

    # ------------------------------------------------------
    # Create images for finding first ear
    # ------------------------------------------------------
    options['save'] = False
    options['continue'] = True
    options['draw_faces'] = False
    options['draw_edges'] = False
    colors = [randomColor()]
    options['annotate_verts'] = True
    i = 0
    vlist = vertices
    ind, vlist = triangulate(vertices, i)
    # First three are not valid, fourth is an ear
    face_colors = [color_table['red'], color_table['red'], color_table['red'], color_table['blue']]
    while i < len(vertices):
        fig = drawTriangulation(vertices, normals,
                                ind,
                                '',
                                directory='simple-alt',
                                colors=colors,
                                options=options)
        fcolor = color_table['red']
        if len(face_colors)>i:
            fcolor = face_colors[i]
        j = i-1
        if j<0:
            j = len(vertices) + j
        k = (i+1) % len(vertices)
        triplet = [vertices[j,:], vertices[i,:], vertices[k,:]]
        drawInternalAngle(triplet, fig, z_offset=1)
        drawConnectingEdge(triplet, fig)
        drawFace(triplet, fig, fcolor)
        ax = fig.gca()
        ax.set_xlim(options['xlim'][0], options['xlim'][1])
        ax.set_ylim(options['ylim'][0], options['ylim'][1])
        plt.tight_layout()
        name = 'simple_alt-{:02d}.png'.format(i)
        filepath = os.path.join('./', directory, 'img', name)
        plt.savefig(filepath)
        name = 'simple_alt-{:02d}.svg'.format(i)
        filepath = os.path.join('./', directory, 'svg', name)
        plt.savefig(filepath)
        plt.close()
        i += 1

    options['save'] = True
    options['continue'] = False
    indices, vertlist = triangulate(vertices, 1)
    vertices_left = []
    if vertlist:
        vertices_left = np.zeros([len(vertlist), 2])
        for index, i in enumerate(vertlist):
            vertices_left[index,:] =  vertices[i,:]
    verts = vertices_left
    indices, vertlist = triangulate(verts)
    options['draw_polygon'] = True
    options['draw_vertices'] = False
    options['draw_edges'] = False
    options['draw_faces'] = False
    options['annotate_verts'] = False
    options['save'] = False
    options['continue'] = True
    fig = drawTriangulation(verts,
                            normals, 
                            indices,
                            options=options)
    triplet = [vertices[2,:], vertices[3,:], vertices[4,:]]
    drawFace(triplet, fig, color_table['blue'], z_offset=0)
    options['draw_polygon'] = False
    options['draw_edges'] = False
    options['draw_vertices'] = True
    options['annotate_verts'] = True
    options['save'] = True
    options['continue'] = False
    fig = drawTriangulation(vertices,
                            normals,
                            indices,
                            'iteration-1',
                            'simple-alt',
                            fig=fig,
                            options=options) 

def exampleSimple():
    """
        Simple polygon
    """
    options = getDefaultOptions()

    vertices, normals, indices = generateSimple()

    n = len(indices)
    options['save'] = False
    options['continue'] = True
    fig = drawTriangulation(vertices, normals, indices[0:n//2, :], 'simple', options=options)

    options['face_color'] = color_table['red']
    options['z-offset'] = 5
    drawTriangulation(vertices, normals, indices[n//2:, :], 'simple', fig=fig, options=options)

    options['save'] = True
    options['draw_polygon'] = True
    options['draw_faces'] = False
    options['draw_edges'] = True
    options['draw_vertices'] = True
    options['z-offset'] = 10
    drawTriangulation(vertices, normals, indices, 'simple', fig=fig, options=options)

def exampleHole():
    """
        An illustration of a polygon with a hole.
    """
    vertices, normals, indices = generateSimple()

    options = getDefaultOptions()
    options['save'] = False
    options['continue'] = True
    options['face_color'] = color_table['blue']
    options['annotate_verts'] = True
    fig = drawTriangulation(vertices, normals, indices, options=options)

    options['face_color'] = color_table['bkg']
    options['z-offset'] = 5

    options['save'] = True
    options['continue'] = False
    options['draw_polygon'] = True
    options['draw_faces'] = True
    options['draw_edges'] = False
    options['draw_vertices'] = True
    options['annotate_verts'] = True
    options['reverse-vertex-annotation'] = True
    options['offset-vertex-annotation'] = len(vertices)

    vertices, normals, indices = generateCircle()
    fig = drawTriangulation(vertices, normals, indices, 'hole', fig=fig, options=options)

def exampleNonSimpleFan():
    """
        Non simple polygon with more than two edges connected to vertex.
    """
    options = getDefaultOptions()
    options['draw_polygon'] = False
    options['draw_vertices'] = True
    options['draw_edges'] = True
    options['annotate_faces'] = False
    options['annotate_verts'] = True
    options['edge-line-width'] = 3.0
    options['save'] = True
    options['continue'] = False

    vertices, normals, indices = generateNoneSimple()
    drawTriangulation(vertices, normals, indices, 'nonsimple_0', options=options)

def exampleNonSimpleCrossing():
    """
        Non simple polygon with edge intersections not at vertices.
    """
    options = getDefaultOptions()
    options['max_vertices'] = 5
    options['draw_polygon'] = False
    options['draw_vertices'] = True
    options['draw_edges'] = True
    options['annotate_faces'] = False
    options['annotate_verts'] = True
    options['edge-line-width'] = 3.0
    options['annotate-index-as'] = {0: 1, 1: 0}
    options['save'] = True
    options['continue'] = False

    vertices, normals, indices = generateCrossing()
    drawTriangulation(vertices, normals, indices, 'nonsimple_1', options=options)

def exampleKochSnoflake():
    """
        Koch snowflake
    """
    options = getDefaultOptions()
    options['face_color'] = None
    options['max_vertices'] = -1
    options['draw_polygon'] = True
    options['draw_vertices'] = False
    options['draw_edges'] = False
    options['annotate_faces'] = False
    options['annotate_verts'] = False
    options['save'] = True
    options['continue'] = False
    
    vertices = kochSnowflake(3)
    ind, vlist = triangulate(vertices)
    options['annotate_verts'] = False
    drawTriangulation(vertices, None, ind, 'koch-snowflake', options=options)

    # ------------------------------------------------------
    # Koch animation
    # ------------------------------------------------------
    colors = [randomColor()]
    options['annotate_verts'] = False
    i = 1
    vlist = vertices
    ind = []
    while len(vlist)>2:
        ind, vlist = triangulate(vertices, i)
        name = 'koch-snowflake-{:03d}'.format(i)
        drawTriangulation(vertices, None,
                          ind,
                          name,
                          directory='koch-sequence',
                          colors=colors,
                          options=options)
        colors.append(randomColor())
        i += 1
    options['face_color'] = color_table['blue']
    drawTriangulation(vertices, None,
                      ind,
                      'koch-snowlake-final',
                      directory='koch-sequence',
                      options = options)
def exampleBunny():
    """
        Bunny polygon
    """
    options = {}
    options['face_color'] = color_table['blue']
    options['max_vertices'] = -1
    options['draw_polygon'] = True
    options['draw_vertices'] = True
    options['draw_edges'] = True
    options['annotate_faces'] = False
    options['annotate_verts'] = False
    to_poly = ImageMaskContourToPolygon('resources/bunny_mask_large.png')

    vertices = to_poly.execute()
    ind, vlist = triangulate(vertices)
   
    options['face_color'] = color_table['bkg']
    options['draw_polygon'] = True
    options['draw_vertices'] = False
    options['draw_edges'] = False
    options['save'] = True
    options['continue'] = False
    drawTriangulation(vertices, None, ind, 'bunny-triangulation-contour', options=options)
    
    options['face_color'] = None
    options['draw_polygon'] = True
    options['draw_vertices'] = False
    options['draw_edges'] = False
    options['save'] = True
    options['continue'] = False
    drawTriangulation(vertices, None, ind, 'bunny-triangulation-triangles', options=options)
    
    options['face_color'] = color_table['blue']
    options['draw_polygon'] = True
    options['draw_vertices'] = False
    options['draw_edges'] = False
    options['save'] = True
    options['continue'] = False
    drawTriangulation(vertices, None, ind, 'bunny-triangulation-filled', options=options)

# ==============================================================================
#
# Main
#
# ==============================================================================

if __name__ == "__main__":

    initRandomColor()
    exampleSimpleAlt()
    #exampleHole()
    #exampleNonSimpleFan()
    #exampleNonSimpleCrossing()
    #exampleKochSnoflake()
    #exampleBunny()
